module modernc.org/bench

require (
	github.com/edsrzf/mmap-go v0.0.0-20170320065105-0bce6a688712 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20170806203942-52369c62f446 // indirect
	golang.org/x/tools v0.0.0-20181109142811-879803d4ad65
	modernc.org/gc v1.0.0
	modernc.org/mathutil v1.0.0
	modernc.org/token v1.0.0
)
